User Environment
----------------

APT_CONFIG=/var/lib/sbuild/apt.conf
DEB_BUILD_OPTIONS=parallel=4
HOME=/sbuild-nonexistent
LANG=C.UTF-8
LC_ALL=C.UTF-8
LOGNAME=buildd
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games
SCHROOT_ALIAS_NAME=build-PACKAGEBUILD-19127329
SCHROOT_CHROOT_NAME=build-PACKAGEBUILD-19127329
SCHROOT_COMMAND=env
SCHROOT_GID=2501
SCHROOT_GROUP=buildd
SCHROOT_SESSION_ID=build-PACKAGEBUILD-19127329
SCHROOT_UID=2001
SCHROOT_USER=buildd
SHELL=/bin/sh
TERM=unknown
USER=buildd
V=1

dpkg-buildpackage
-----------------

dpkg-buildpackage: info: source package gnat
dpkg-buildpackage: info: source version 9ubuntu2
dpkg-buildpackage: info: source distribution focal
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   dh_clean
 debian/rules binary
dh binary
   dh_update_autotools_config
   dh_autoreconf
   create-stamp debian/debhelper-build-stamp
   dh_testroot
   dh_prep
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_dwz
   dh_strip
   dh_makeshlibs
   dh_shlibdeps
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
INFO: pkgstriptranslations version 144
INFO: pkgstriptranslations version 144
pkgstriptranslations: processing gnat (in debian/gnat); do_strip: , oemstrip: 
pkgstriptranslations: processing gnat-doc (in debian/gnat-doc); do_strip: , oemstrip: 
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgstripfiles: processing control file: debian/gnat/DEBIAN/control, package gnat, directory debian/gnat
pkgstripfiles: processing control file: debian/gnat-doc/DEBIAN/control, package gnat-doc, directory debian/gnat-doc
INFO: pkgstripfiles: waiting for lock (gnat-doc) ...
pkgstripfiles: Truncating usr/share/doc/gnat/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 4 cpus) for package gnat ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'gnat' in '../gnat_9ubuntu2_amd64.deb'.
pkgstripfiles: Truncating usr/share/doc/gnat-doc/changelog.gz to topmost ten records
pkgstripfiles: Disabled PNG optimization for -doc package gnat-doc (to save build time)
dpkg-deb: building package 'gnat-doc' in '../gnat-doc_9ubuntu2_all.deb'.
 dpkg-genbuildinfo --build=binary
 dpkg-genchanges --build=binary -mLaunchpad Build Daemon <buildd@lcy01-amd64-016.buildd> >../gnat_9ubuntu2_amd64.changes
dpkg-genchanges: info: binary-only upload (no source code included)
 dpkg-source --after-build .
